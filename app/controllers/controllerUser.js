const { default: mongoose } = require("mongoose");
const userModel = require("../models/userModel")

// Create a User
const createUser = async (req, res) => {
    let body = req.body;

    if (!body.email) {
        return res.status(400).json({
            message: `fullname is required`
        })
    }
    if (!body.phone ) {
        return res.status(400).json({
            message: `phone is required`
        })
    }

    try {

        let newUser = {
            _id: new mongoose.Types.ObjectId(),
            fullName: body.fullName,
            email: body.email,
            address: body.address,
            phone: body.phone,
        }

        const createUser = await userModel.create(newUser)

        return res.status(201).json({
            status: "Create User successfully",
            createUser
        })

    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

// Get all User
const getAllUser = (req, res) => {
    userModel.find()
        .then(data => {
            return res.status(200).json({
                status: "Get all User SucessFully",
                data
            })
        })
        .catch(err => {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        })
}
// get User ById

const getUserById = (req, res) => {
    let id = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Id is invalid",
        })
    }
    userModel.findById(id)
        .then(data => {
            return res.status(200).json({
                message: "Get User by id sucessfully",
                data,
            })
        })
        .catch(err => {
            return res.status(500).json({
                message: "Internal server error",
                err: err.message
            })
        })
}

// Update User By Id 

const updateUserById = (req, res) => {
    let id = req.params.userId;
    let body = req.body

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Id is invalid",
        })
    }
    if (!body.fullName) {
        return res.status(400).json({
            message: `fullname is required`
        })
    }
    if (!body.email) {
        return res.status(400).json({
            message: `email is required`
        })
    }
    if (!body.phone ) {
        return res.status(400).json({
            message: `phone is required`
        })
    }
    let newUserUpdate = {
        fullname: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone,
    }
    userModel.findByIdAndUpdate ( id, newUserUpdate) 
    .then(data => {
        return res.status(200).json({
            message: `Update user sucessfully`,
            data
        })
    })
    .catch(error => {
        return res.status(500).json({
            message: `Internal server error`,
            err: error.message
        })
    })
}

const deleteUserById = (req, res) => {
    let id = req.params.userId;
    let body = req.body

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Id is invalid",
        })
    }

    userModel.findByIdAndDelete ( id) 
    .then(data => {
        return res.status(200).json({
            message: `Delete User sucessfully`,
            data
        })
    })
    .catch(error => {
        return res.status(500).json({
            message: `Internal server error`,
            err: error.message
        })
    })

}

module.exports = { createUser, getAllUser, getUserById, updateUserById, deleteUserById }