const getAllCoures = (req, res, next) => {
    console.log("Get all course");
    next();
}
const getCoures = (req, res, next) => {
    console.log("Get a course");
    next();
};
const postCoures = (req, res, next) => {
    console.log("Post a course");
    next();
}
const putCoures = (req, res, next) => {
    console.log("Put a course");
    next();
}
const deleteCoures = (req, res, next) => {
    console.log("Delete a course");
    next();
}

module.exports = {
    getAllCoures,
    getCoures,
    postCoures,
    putCoures,
    deleteCoures
}
