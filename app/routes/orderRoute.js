const express =  require("express");
const { createOrer, getAllOrder, getOrderByID, updateOrder, deleteOrder } = require("../controllers/controllerOrder");

const router = express.Router();

router.post("/orders/:userId/orders",createOrer )
// get all order
router.get("/orders",getAllOrder )

// get order by id
router.get("/orders/:orderId",getOrderByID )

// update
router.put("/orders/:orderId",updateOrder )

router.delete("/orders/:orderId",deleteOrder )


module.exports = router