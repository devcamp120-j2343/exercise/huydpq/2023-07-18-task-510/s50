const express = require("express");
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require("../controllers/controllerUser");

const router = express.Router();

router.post("/user",createUser)

router.get("/user", getAllUser)

router.get("/user/:userId", getUserById)

router.put("/user/:userId", updateUserById);

router.delete("/user/:userId", deleteUserById)

module.exports = router